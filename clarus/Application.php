<?php

namespace clarus;

class Application extends scl_Singleton {

    protected static $instance = NULL;
    protected $routers = array();
    protected $controller = NULL;
    protected $template = NULL;

    /**
     * @return Application
     */
    public static function i() {
        if (!(self::$instance instanceof self)) {
            self::$instance = new Application();
        }
        return self::$instance;
	    }

    protected function __construct() {
        
    }

    public function addRoute(router_Router $router) {
        $this->routers[] = $router;
    }

    public function start() {
        if (sizeof($this->routers) == 0) {
            throw new \UnderflowException('Application started without any router');
        }

        foreach ($this->routers as &$router) {
            if ($router->isMatch()) {
                $cls = 'controller_' . $router->getClass();
                if (class_exists($cls)) {
                    $this->controller = new $cls();
                    $this->controller->init();
                } else {
                    throw new ApplicationException('Requested controller [' . $cls . '] not found');
                }

                $mtd = $router->getMethod();
                if (method_exists($this->controller, $router->getMethod())) {
                    $this->controller->$mtd($router->getParams());
                } else {
                    throw new ApplicationException('Requested method [' . $cls . '::' . $mtd . '] not found');
                }

                $this->template = Env::i()->getPATH() . DIRECTORY_SEPARATOR . 'tpl' . DIRECTORY_SEPARATOR . str_replace('_', DIRECTORY_SEPARATOR, mb_strtolower($cls . $mtd));

                break;
            }
        }
    }

    public function render() {
        header('Content-type: '.Response::i()->getContentType());
        if (Response::i()->getContentType() == Response::TYPE_JSON) {
            $responsePacket = ['_status'=>['msg'=>'ok','code'=>1],'_content'=>Response::i()->exportVars(), '_form'=>  Response::i()->getFormErrors()];
            echo json_encode($responsePacket);
        } else {
            $normalTpl = $this->template . '.tpl';
            $leafTpl = $this->template . '.leaf';

            // message workaround
            if (isset($_SESSION['messages'])) {
                while (sizeof($_SESSION['messages']) > 0) {
                    echo '<div class="msg">' . array_pop($_SESSION['messages']) . '</div>';
                }
            }

            if (file_exists($normalTpl) || file_exists($leafTpl)) {
                if (file_exists($normalTpl)) {
                    include_once $normalTpl;
                }
                if (file_exists($leafTpl)) {
                    include_once $leafTpl;
                }
            } else {
                throw new ApplicationException('No tpl or leaf template found [' . $this->template . ']');
            }
        }
    }

    public function redirect($url) {
        if(Response::i()->getContentType() == Response::TYPE_JSON) {
            header('HTTP/1.1 232 X-Redirect');
            header('X-Redirect: '.$url);
        } else {
            header('Location: ' . $url);
        }
    }

}