<?php

namespace clarus;

class security_User extends scl_Singleton {

    protected static $instance = NULL;
    protected $username = NULL;
    protected $id = NULL;

    /**
     * @return security_User
     */
    public static function i() {
        if (self::$instance instanceof self) {
            return self::$instance;
        } else if (isset($_SESSION['user'])) {
            return self::$instance = unserialize($_SESSION['user']);
        }
        throw new security_autentification_Exception('Not autentified');
    }

    public static function login($username, $password) {
        $r = DB::i()->query(
                        sprintf("SELECT * FROM t_user WHERE username = '%s' AND password = '%s'", $username, md5($password))
                )->fetch(\PDO::FETCH_ASSOC);

        if ($r == FALSE) {
            throw new security_autentification_Exception('Bad username or password');
        } else {
            self::$instance = new security_User($r['username'], $r['id_user']);
        }
    }

    public function getUsername() {
        return $this->username;
    }

    public function getId() {
        return $this->id;
    }

    public function getPriceLevel() {
        // tady v realu nebude ID ale nacteny price level z tabulky user
        return $this->id;
    }

    protected function __construct($username, $id) {
        $this->username = $username;
        $this->id = $id;
    }

    public function __destruct() {
        $_SESSION['user'] = serialize($this);
    }

}