<?php

namespace clarus;

class DB extends scl_Singleton {

    protected static $instance = NULL;
    
    /**
     * @var \PDO
     */
    public $resource = NULL;

    /**
     * @return DB
     */
    public static function i() {
        if (!(self::$instance instanceof self)) {
            self::$instance = new DB();
        }
        return self::$instance;
    }

    protected function __construct() {
        $this->resource = new \PDO('mysql:host=db.dev.local;port=3306;dbname=ebddev', 'root', 'ebd', array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
    }
    
    /**
     * @param string $sql
     * @param array $params
     * @return \PDOStatement
     */
    public function query($sql,array $params = array()) {
        $statement = $this->resource->prepare($sql);
        foreach ($params as $key => $value) {
            $statement->bindParam($key, $value);
        }
        $statement->execute();
        return $statement;
    }

}