<?php

namespace clarus;

class router_Basic extends router_Router {

    protected $url = NULL;
    protected $parts = array();

    public function __construct() {
        if(isset($_SERVER['REDIRECT_URL'])) {
            $this->url = $_SERVER['REDIRECT_URL'];
        } else {
            $this->url = '/';
        }
        $this->parts = explode('/', trim($this->url, '/'), 3);
    }

    public function getClass() {
        if(isset($this->parts[0]) && strlen($this->parts[0])) {
            return ucfirst($this->parts[0]);
        } else {
            return 'Default';
        }
    }

    public function getMethod() {
        if(isset($this->parts[1])) {
            return '_'.mb_strtolower($this->parts[1]);
        } else {
            return '_default';
        }        
    }

    public function getParams() {
        if(isset($this->parts[2])) {
            return $this->parts[2];
        } else {
            return NULL;
        }        
    }
    
    public function isMatch() {
        return TRUE;
    }


}