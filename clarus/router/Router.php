<?php

namespace clarus;

abstract class router_Router {
    
    abstract public function getClass();
    
    abstract public function getMethod();
    
    abstract public function getParams();
    
    abstract public function isMatch();
    
}