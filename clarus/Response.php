<?php

namespace clarus;

class Response extends scl_Singleton {

    const TYPE_HTML = 'text/html';
    const TYPE_JSON = 'application/json';
    
    protected static $instance = NULL;
    protected $tpl = NULL;
    protected $vars = array();
    protected $contentType = self::TYPE_HTML;
    protected $formErrors = array();

    /**
     * @return Response
     */
    public static function i() {
        if (!(self::$instance instanceof self)) {
            self::$instance = new Response();
        }
        return self::$instance;
    }
    
    public function setContentType($contentType) {
        $this->contentType = $contentType;
    }
    
    public function getContentType() {
        return $this->contentType;
    }

    public function setVar($name, $var) {
        $this->vars[$name] = $var;
    }

    public function getVar($name) {
        return $this->vars[$name];
    }
    
    public function exportVars() {
        return $this->vars;
    }
    
    public function getFormErrors() {
        return $this->formErrors;
    }

    public function setFormErrors($formName, array $formErrors) {
        $this->formErrors[$formName] = $formErrors;
        return $this;
    }

}