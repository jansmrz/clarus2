<?php

namespace clarus;

abstract class controller_Controller {

    /**
     * @var Response
     */
    protected $response = NULL;

    final public function __construct() {
        $this->response = Response::i();
        foreach (class_uses($this) as $trait) {
            if (method_exists($trait, 'initComponent')) {
                foreach ($trait::initComponent() as $key => $value) {
                    $this->$key = $this->$value();
                    $this->response->setVar($key, $this->$key);
                }
            }
        }
    }

    public function init() {
        
    }

}