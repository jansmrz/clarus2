<?php

namespace clarus;

abstract class form_Form implements IPrintable {

    use scl_HtmlElement;

    protected $template = NULL;
    protected $items = array();
    protected $name = NULL;
    protected $errors = array();
    protected $values = array();

		abstract protected function create();

    public function __construct() {
        $this->template = Env::i()->getPATH() . DIRECTORY_SEPARATOR . 'tpl' . DIRECTORY_SEPARATOR . 'form' . DIRECTORY_SEPARATOR . 'form.php';
				$this->create();
    }

    public function printHtml() {
        include $this->template;
    }

    public function setTemplate($template) {
        $this->template = Env::i()->getPATH() . DIRECTORY_SEPARATOR . 'tpl' . DIRECTORY_SEPARATOR . $template;
    }

    public function addItem(form_item_Item $item) {
        $item->setForm($this);
        $this->items[$item->getName()] = $item;
    }

    public function getName() {
        return $this->name;
    }

    public function addFormError($error) {
        $this->errors['_form'][] = $error;
        Response::i()->setFormErrors($this->getName(), $this->errors);
        return $this;
    }

    public function addItemError($error, $item) {
        $this->errors['_items'][$item][] = $error;
        Response::i()->setFormErrors($this->getName(), $this->errors);
        return $this;
    }

    public function getValues() {
        return $this->values;
    }

    public function onSubmit() {
        if ($this->getHTMLAttribute('method') != 'post') {
            if (isset($_GET['form'][$this->getName()])) {
                $this->values = $_GET['form'][$this->getName()];
            } else {
                return FALSE;
            }
        } else {
            if (isset($_POST['form'][$this->getName()])) {
                $this->values = $_POST['form'][$this->getName()];
            } else {
                return FALSE;
            }
        }

        foreach ($this->items as $key => $item) {
            if ($item->getHTMLAttribute('required') != NULL && strlen($this->values[$key]) == 0) {
                $this->errors[$key][] = _('Musi byt vyplneno');
            }
            /* if($item->getHTMLAttribute('pattern') != NULL && preg_match('~'.$item->getHTMLAttribute('pattern').'~', $values[$key])) {
              $this->errors[$key][] = _('Spatny format');
              } */
        }

        return TRUE;
    }

    public function isValid() {
        $this->onSubmit();
        //var_dump($this->errors);
        if (sizeof($this->errors) > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}