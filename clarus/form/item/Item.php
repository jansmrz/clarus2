<?php

namespace clarus;

abstract class form_item_Item implements IPrintable {

    use scl_HtmlElement;
    
    const TYPE_TEXT = 1;
    const TYPE_PASS = 2;
    const TYPE_SUBMIT = 3;
    
    protected $name = NULL;
    protected $htmlType = NULL;
    protected $label = NULL;
    
    protected $defaultValue = NULL;
    protected $value = NULL;
    
    protected $form = NULL;

    public static function create($name, $type) {
        switch ($type) {
            case self::TYPE_TEXT:
                $item = new form_item_Text($name);
                break;
            case self::TYPE_PASS:
                $item = new form_item_Pass($name);
                break;
            case self::TYPE_SUBMIT:
                $item = new form_item_Submit($name);
                break;
            default:
                throw new \UnexpectedValueException('Invalid type');
        }
        return $item;
    }

    protected function __construct($name) {
        $this->name = $name;
    }
    
    public function getName() {
        return $this->name;
    }

    public function getLabel() {
        return $this->label;
    }

    public function setLabel($label) {
        $this->label = $label;
        return $this;
    }

    public function getDefaultValue() {
        return $this->defaultValue;
    }

    public function setDefaultValue($defaultValue) {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    public function getValue() {
        return $this->value;
    }

    /**
     * @return form_Form
     */
    public function getForm() {
        return $this->form;
    }
    
    public function setForm(form_Form $form) {
        $this->form = $form;
        return $this;
    }

}