<?php

namespace clarus;

class form_item_Submit extends form_item_Item {
    
    public function __construct($name) {
        parent::__construct($name);
        $this->htmlType = 'submit';
    }
    
    public function printHtml() {
        include Env::i()->getPATH().DIRECTORY_SEPARATOR.'tpl'.DIRECTORY_SEPARATOR.'form'.DIRECTORY_SEPARATOR.'input.php';
    }

    public function setTemplate($template) {
        
    }
    
}