<?php

namespace clarus;

class Env {
    
    private static $instance = NULL;
    
    protected $PATH = NULL;
    
    /**
     * @return Env
     */
    public static function i() {
        if(!(self::$instance instanceof Env)) {
            self::$instance = new Env();
        }
        return self::$instance;
    }
    
    protected function __construct() {
        if(realpath('..') === FALSE) {
            $this->PATH = '..';
        } else {
            $this->PATH = realpath('..');
        }
    }
    
    public function getPATH() {
        return $this->PATH;
    }
}