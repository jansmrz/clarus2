<?php

namespace clarus;

interface IPrintable {
    
    public function printHtml();
    
    public function setTemplate($template);
    
}