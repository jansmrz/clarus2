<?php

namespace clarus;

abstract class scl_Utils {
    
    const DUMP_HTML = 1;
    const DUMP_CLI = 2;
    
    const HTML_PAD_SIZE = 3;
    const HTML_STYLE_KEY = 'color: RoyalBlue;';
    const HTML_STYLE_VAR_TYPE = 'color: FireBrick; font-weight: bold;';
    const HTML_STYLE_STRING = 'color: Gold;';

    public static function var_dump($var, $mode = self::DUMP_HTML) {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }
    
    protected static function printVar($var) {
        $rtn = '';
        switch (gettype($var)) {
            case 'array':
                $rtn .= '<div style="'.self::HTML_STYLE_VAR_TYPE.'">Array {</div>';
                foreach ($var as $key => $value) {
                    $rtn .= str_repeat('&nbsp;', self::HTML_PAD_SIZE).'<span style="'.self::HTML_STYLE_KEY.'">'.$key.'</span>';
                    $rtn .= ' => ';
                    $rtn .= self::printVar($value);
                    $rtn .= '<br />';
                }
                $rtn .= '<div style="'.self::HTML_STYLE_VAR_TYPE.'">}</div>';
                break;
            case 'string':
                return '<span style="'.self::HTML_STYLE_STRING.'">STRING('.  mb_strlen($var).') '.$var.'</span>';
                break;
            default:
                //return print_r($var, TRUE);
                break;
        }
        return $rtn;
    }
    
}