<?php

namespace clarus;

abstract class scl_Singleton {
    
    final public function __clone() {
        throw new \LogicException('Nelze klonovat singleton');
    }
    
}