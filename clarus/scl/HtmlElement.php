<?php

namespace clarus;

trait scl_HtmlElement {
    
    protected $HTMLAttributes = array();
    
    public function setHTMLAttribute($key, $value) {
        if(key_exists($key, $this->HTMLAttributes)) trigger_error (sprintf ("Attribute %s already exists, overwriting...", $key), E_USER_WARNING);
        $this->HTMLAttributes[$key] = $value;
        return $this;
    }
    
    public function getHTMLAttribute($key) {
        if(isset($this->HTMLAttributes[$key])) {
            return $this->HTMLAttributes[$key];
        } else {
            return NULL;
        }
    }
    
}