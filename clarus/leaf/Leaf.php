<?php

namespace clarus;

class leaf_Leaf {

    protected $parsed = array();

    public function __construct(\SplFileObject $file) {
        foreach ($file as $num => $line) {
            $line = trim($line);
            // preskocime komentar
            if (substr($line, 0, 2) == '//')
                continue;

            echo 'old:' . $line . '<br />';
            echo 'new:' . preg_replace_callback('~{(/|\$|)[a-zA-Z0-9_]+([a-zA-Z0-9 \|\=\$]+)}~U', array('self', 'dispatcher'), $line) . '<br />';
        }
    }

    protected function dispatcher($arg) {
        $inner = trim($arg[0], '{}');
        if ($inner{0} == '$') {
            $parts = explode('|', $inner);

            return htmlentities('<?= \clarus\Response::i()->getVar(\'' . ltrim($parts[0], '$') . '\') ?>');
        }

        return $inner;
    }

}