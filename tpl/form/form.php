<form <?php foreach ($this->HTMLAttributes as $key => $value){ echo $key.'="'.$value.'" '; } ?> >
    <table>
        <?php foreach ($this->items as $key => $value): ?>
        <tr>
            <td><?= $value->getLabel() ?></td>
            <td><?= $value->printHtml() ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</form>