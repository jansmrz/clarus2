<form action="/default/calc" method="get" target="#content">
    <table style="width: 100%">
        <tr>
            <th></th>
            <th>pocet/zkusenost</th>
            <th></th>
            <th>pocet/zkusenost</th>
            <th>PWR</th>
            <th>Utok</th>
            <th>Stit(&plusmn;%)</th>
            <th>Zivoty</th>
        </tr>
        <?php foreach(\clarus\Response::i()->getVar('units') as $unit): ?>
        <tr>
            <td><label for="<?= $unit->getName() ?>_1_name"><?= $unit->getName() ?></label></td>
            <td>
                <input id="<?= $unit->getName() ?>_1_name" placeholder="empty!" pattern="[0-9]{1,4}" value="0" name="form[units][0][<?= $unit->getName() ?>][count]" />
                <input id="<?= $unit->getName() ?>_1_exp" type="number" min="1" max="100" required="required" pattern="[0-9]{1,3}" value="50" name="form[units][0][<?= $unit->getName() ?>][exp]" />
            </td>
            <td><label for="<?= $unit->getName() ?>_2"><?= $unit->getName() ?></label></td>
            <td>
                <input id="<?= $unit->getName() ?>_2_name" placeholder="empty!" pattern="[0-9]{1,4}" value="0" name="form[units][1][<?= $unit->getName() ?>][count]" />
                <input id="<?= $unit->getName() ?>_2_exp" type="number" min="1" max="100" required="required" value="50" name="form[units][1][<?= $unit->getName() ?>][exp]" />
            </td>
            <td><?= $unit->getPower() ?></td>
            <td><?= $unit->getDammage() ?></td>
            <td><?= $unit->getShield() ?>%</td>
            <td><?= $unit->getLives() ?></td>
        </tr>
        <?php endforeach; ?>
        <?php for($i=1;$i<=4;$i++): ?>
        <tr>
            <td><?= $i ?>.kolo</td>
            <td>
                <select name="form[spells][0][<?= $i ?>][type]">
                    <option></option>
                    <option value="Zran">Zraň</option>
                    <option value="Zran">Zraň</option>
                </select>
                <input name="form[spells][0][<?= $i ?>][exp]" type="number" min="0" max="100" value="10" />%
            </td>
            <td><?= $i ?>.kolo</td>
            <td colspan="5">
                <select name="form[spells][1][<?= $i ?>][type]">
                    <option></option>
                    <option value="Zran">Zraň</option>
                </select>
                <input name="form[spells][1][<?= $i ?>][exp]" type="number" min="0" max="100" value="10" />%
            </td>
        </tr>
        <?php endfor; ?>
        <tr>
            <td colspan="8" style="text-align: center;"><input type="submit" value="budiz" /></td>
        </tr>
    </table>
</form>