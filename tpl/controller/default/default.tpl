<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" dir="ltr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>project X, The Game, whatever you want....</title>
        <link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />
        <script type="text/javascript" src="/js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="/js/hydronium/hydronium.js"></script>
        <script type="text/javascript" src="/js/hydronium/delegations.js"></script>
        <script type="text/javascript" src="/js/hydronium/loadable/loadable.js"></script>
        <script type="text/javascript" src="/js/hydronium/loadable/delegations.js"></script>
        <script type="text/javascript" src="/js/hydronium/form/form.js"></script>
        <script type="text/javascript" src="/js/hydronium/form/delegations.js"></script>
        <script type="text/javascript" src="/js/hydronium/ui/ui.js"></script>
        <script type="text/javascript" src="/js/hydronium/ui/delegations.js"></script>
        <script type="text/javascript" src="/js/ui.js"></script>
        <link rel="stylesheet" href="/css/screen.css" type="text/css" media="screen, projection" />
    </head>
    <body>
				<div id="bodyBox">
						<div id="loginBox">
								<?php echo \clarus\Response::i()->getVar('loginForm')->printHTML() ?>
						</div>
				</div>
    </body>
</html>