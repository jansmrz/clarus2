/**
 * Hydronium engine core
 * @author Jan Smrz <jan-smrz@jan-smrz.cz>
 * @version 0.2
 */

var requestPacket = function(xhr) {
    try {
        data = JSON.parse(xhr.responseText);
        this.status = xhr.status;
        this.content = data._content;
        this.form = data._form;
    } catch (err) {
        this.status = 200;
        this.content = {};
        this.form = {};
    }
};

Array.prototype.max = function() {
    var max = this[0];
    var len = this.length;
    for (var i = 1; i < len; i++)
        if (this[i] > max)
            max = this[i];
    return max;
};

Array.prototype.min = function() {
    var min = this[0];
    var len = this.length;
    for (var i = 1; i < len; i++)
        if (this[i] < min)
            min = this[i];
    return min;
};

Array.prototype.sum = function() {
    var sum = 0;
    var len = this.length;
    for (var i = 0; i < len; i++)
        sum += this[i];
    return sum;
};

Array.prototype.unique = function(a) {
    return function() {
        return this.filter(a)
    }
}(function(a, b, c) {
    return c.indexOf(a, b + 1) < 0
});

/**
 * Hydronium engine core
 * @author Jan Smrz <jan-smrz@jan-smrz.cz>
 * @version 0.1.1
 */
(function($) {
    /**
     * setup
     */
    $(document).ready(function() {
        // fire spawn event on all elements when document is ready
        $('*').trigger('spawn');

    });

    /**
     * finds closest higher container by given context
     * @return this
     */
    $.fn.closestContainer = function() {
        return $(this).closest('._container');
    };

    /**
     * @param state name of the state param
     */
    $.fn.setState = function(state) {
        return $(this).trigger('setState', state);
    }

    /**
     * @param determines the occurance of a given state
     */
    $.existsState = function(state) {
        var cState = (state.substr(0, 2) == '__' ? state : '__' + state);
        return ($('.' + cState).length > 0 ? true : false);
    }

    $.fn.hasState = function(state) {
        return $(this).hasClass('__' + state);
    }

    /**
     * @param state name of the state param
     */
    $.fn.invalidState = function(state) {
        return $(this).trigger('invalidState', state);
    }
    /**
     * sets feature name with predefined prefix
     * @param string feature
     */
    $.fn.setFeature = function(feature) {
        $(this).addClass('__' + feature);
    }
    /**
     * removes feature name with predefined prefix
     * @param string feature
     */
    $.fn.invalidateFeature = function(feature) {
        $(this).removeClass('__' + feature);
    }
    /**
     * determines whether element has specific feature
     * @param string feature
     */
    $.fn.hasFeature = function(feature) {
        return ($(this).hasClass('__' + feature) ? true : false);
    }

    /**
     * @param type type of request (eg. GET. POST...)
     * @param url handler url
     * @param optional data as JSON
     * @param context
     * @return this
     */
    $.fn.request = function(type, url, data) {
        // zjistime jestli mame url v cache
        var cache = document.cache.urls[url];
        if (cache !== undefined) {
            //console.info('using cache for url '+url);
            $(this).trigger('requestSuccess', cache);
            var context = this;
            setTimeout(function() {
                $(context).trigger('requestComplete', new requestPacket());
            }, 50);
            return $(this);
        }

        $.ajax({
            type: type,
            url: url,
            data: data,
            cache: false,
            context: this,
            async: true,
            complete: function(xhr) {
                $(this).trigger('requestComplete', new requestPacket(xhr));
                clearInterval(this.longOperationTimeout);
                $(this).invalidState('longLoading');
            },
            success: function(data, status, xhr) {
                console.info(this);
                try {
                    switch (xhr.status) {
                        case 232:
                            // custom redirect by not existing HTTP code 232 Redirect
                            window.location = xhr.getResponseHeader('X-Redirect');
                            break;
                        default:
                            if (data instanceof Object) {
                                $(this).trigger('requestSuccess', data.content);
                            } else {
                                $(this).trigger('requestSuccess', data);
                            }
                    }
                } catch (err) {
                    $(this).error(err);
                    $(this).trigger('requestError', xhr, status);
                }
            },
            error: function(xhr) {
					var packet = new requestPacket(xhr);
            	$(this).trigger('requestError', packet);
					if(packet.status == 500) {
						alert('V aplikaci doslo k neocekavane chybe');
					}
            },
            beforeSend: function(xhr) {
                $(this).trigger('requestSent', new requestPacket(xhr));
                this.longOperationTimeout = setTimeout(function() {
                    $(this).setState('longLoading');
                }.bind(this), 1000);
            }
        });
        return $(this);
    }

    $.fn.historyPush = function(content) {
        if (content.length == 0)
            $(this).error('Content for history must be longer than 0!');
        h = $(this).data('history');
        if (h == undefined)
            h = [];
        h.push(content);
        $(this).data('history', h);
    }

    $.fn.historyPull = function() {
        h = $(this).data('history');
        if (h == undefined)
            return undefined;
        return h.pop();
    }

    $.fn.historyLength = function() {
        h = $(this).data('history');
        if (h == undefined) {
            return 0;
        } else {
            return h.length;
        }
    }

    $.fn.historyClear = function() {
        $(this).data('history', undefined);
    }

    /* ---------- ERROR REPORTING --------- */

    $.fn.log = function() {
        if (window.console && console.log) {
            var args = Array.prototype.slice.call(arguments, 0);
            console.log('%c' + args.join(' '), 'color: blue', $(this));
        }
        return $(this);
    };

    $.fn.info = function() {
        if (window.console && console.log) {
            var args = Array.prototype.slice.call(arguments, 0);
            console.info('%c' + args.join(' '), 'color: green', $(this));
        }
        return $(this);
    };

    $.fn.warn = function() {
        if (window.console && console.log) {
            var args = Array.prototype.slice.call(arguments, 0);
            console.warn('%c' + args.join(' '), 'color: orange', $(this));
        }
        return $(this);
    };

    $.fn.error = function(label, objs) {
        if (window.console && console.error) {
            var args = Array.prototype.slice.call(arguments, 0);
            console.error('%c' + args.join(' '), 'color: red', $(this));
        }
        return $(this);
    };
    

    
})(jQuery);