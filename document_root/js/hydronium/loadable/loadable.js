/**
 * Loadable plugin
 * @author Jan Smrz <jan-smrz@jan-smrz.cz>
 * @version 0.1
 */
(function($) {
    /**
     * @return this
     */
    $.fn.loadContent = function() {
        // find nearest container
        var container = $(this).findTarget();

        if (container.length == 0) {
            container.error('Container not found');
            return $(this);
        }

        var url = $(this).attr('href');

        if (url === undefined) {
            $(this).error('url not found');
            return $(this);
        }
        var method = 'GET';
        if($(this).attr('method') != undefined) {
            if($(this).attr('method') == 'POST') {
                method = 'POST';
            } else {
                method = 'GET';
            }
        }

        container.setState('loading').request(method, url, {});

        return $(this);
    };
    
    $.fn.findTarget = function() {
        if ($(this).attr('target') == undefined) {
            return $(this).closest('._container');
        } else {
            return $($(this).attr('target')+'');
        }
    }

    $.fn.setTargetAttributes = function(targetAttributes) {
        for (attribute in targetAttributes) {
            if (attribute == 'class') {
                $(this).addClass(targetAttributes[attribute]);
            } else {
                $(this).attr(attribute, targetAttributes[attribute]);
            }
        }
    }

    document.cache = {
        init: function() {
            for (curl in document.cache.urls) {
                $.get(curl, function(data) {
                    document.cache.urls[curl] = data;
                });
            }
        },
        urls: {}
    }
})(jQuery);