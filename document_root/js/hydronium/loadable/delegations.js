/**
 * Loadable plugin delegations
 * @author Jan Smrz <jan-smrz@jan-smrz.cz>
 * @version 0.1
 */
(function($) {
    $(document).on('spawn', '._container:not(.__loading)', function(e) {
        if (e.target == this) {
            $(this).loadContent();
        }
    });
    
    $(document).on('click', '._loadable:not(.__loading):not(._external)', function(e) {
        if (e.target == this) {
            $(this).loadContent();
        }
    });
    
    $(document).on('click', '._loadable._external', function(e) {
        if (e.target == this) {
            window.location = $(this).attr('href');
        }
    });

    // set data on successfull request
    $(document).on('requestSuccess', '.__loading', function(e, data) {
        if (e.target == this) {
            if ($(this).hasFeature('u')) {
                var SPEED = 500;
                if ($(this).find('._grayBox').length > 0) {
                    var oldEl = $(this).find('> *');
                    var newEl = $(data);
                    if ($(this).hasFeature('history')) {
                        $(this).historyPush($(this).find('> *'));
                    }
                    ;

                    oldEl.after(newEl);
                    newEl.find('*').trigger('spawn');
                    newEl.css({'position': 'absolute', 'left': 1000, 'top': 0}).animate({'left': 0}, SPEED);

                    oldEl.animate({'left': -1000}, SPEED, function() {
                        $(this).detach();
                    });
                } else {
                    $(this).html(data).find('*').trigger('spawn');
                }
            } else {
                if ($(this).hasFeature('history')) {
                    $(this).historyPush($(this).find('> *'));
                }
                ;
                $(this).html(data).find('*').trigger('spawn');
            }
            //console.log($(data));
            $(this).trigger('contentChanged', newEl);
        }
    });

    // on complete invalidate loading state
    $(document).on('requestComplete', '.__loading', function(e, data) {
        if (e.target == this) {
            $(this).invalidState('loading');
        }
    });

    // standalone function for history back
    $(document).on('historyBack', '._history', function(e) {
        if (e.target == this) {
            if ($(this).historyLength() > 0) {
                $(this).empty().append($(this).historyPull());
            } else {
                console.info('no more history!');
            }
        }
    })

    $(document).on('click', '._historyBack', function(e) {
        if (e.target == this) {
            $(this).closest('._container._history').trigger('historyBack');
        }
    });

    $(document).ready(function() {
        //document.cache.urls = {'/w_user/edit': false};
        document.cache.urls = {};
        document.cache.init();
    });

})(jQuery);