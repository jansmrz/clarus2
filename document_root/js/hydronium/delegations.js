/**
 * Hydronium engine core
 * @author Jan Smrz <jan-smrz@jan-smrz.cz>
 * @version 0.2
 */
(function($) {
    // default behavioru for all anchors
    $(document).on('click', 'a:not(._default)', function(e) {
        e.preventDefault();
        // tweak na prazdny href aby se neloadla spatne cela stranka
        if ($(this).attr('href') == undefined || $(this).attr('href') == '#' || $(this).attr('href') == '' || $(this).attr('href') == '/') {
            console.error('Nedefinovane href!');
        } else {
            $(this).loadContent();
        }
    });

    // default behaviour for setState event
    $(document).on('setState', '*', function(e, state) {
        if (e.target == this) {
            $(this).addClass('__' + state + ' __mutating');
        }
    });

    // default behaviour for invalidState event
    $(document).on('invalidState', '*', function(e, state) {
        if (e.target == this) {
            $(this).removeClass('__' + state);
            var mutableClassPresent = false;

            for (var i = 0; i < $(this)[0].classList.length; i++) {
                var c = $(this)[0].classList[i];
                if (c == '__mutating')
                    continue;
                if (c.match('^__') != null)
                    mutableClassPresent = true;
            }
            if (!mutableClassPresent)
                $(this).removeClass('__mutating');
        }
    });
    
})(jQuery);