/**
 * Hydronium form
 * @author Jan Smrz <jan-smrz@jan-smrz.cz>
 * @version 0.2
 */
(function($) {

    $(document).on('submit', 'form', function(e) {
        e.preventDefault();
        if (e.target == this) {
            $(this).submitForm();
        }
    });

    $(document).on('requestComplete', 'form', function(e) {
        if (e.target == this) {

        }
    });

    $(document).on('requestError', 'form', function(e, xhr) {
        if (e.target == this) {
            $(xhr.form).each(function(i, el){
               console.log(el);
            });
        }
    });

})(jQuery);