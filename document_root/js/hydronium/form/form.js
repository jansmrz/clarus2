/**
 * Hydronium engine core
 * @author Jan Smrz <jan-smrz@jan-smrz.cz>
 * @version 0.1.1
 */
(function($) {
    
    $.fn.submitForm = function() {
        var url = $(this).attr('action');

        if (url === undefined) {
            $(this).error('action not found!');
            return $(this);
        }
        var method = 'GET';
        if ($(this).attr('method') != undefined) {
            if ($(this).attr('method') == 'POST') {
                method = 'POST';
            } else {
                method = 'GET';
            }
        }

        this.setState('loading').request(method, url, $(this).serializeArray());

        return $(this);
    }

})(jQuery);