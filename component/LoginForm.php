<?php

namespace component;

class LoginForm extends \clarus\form_Form {

    public function create() {
				$this->name = 'login';
        $this->setHTMLAttribute('action', '/user/login');
        $this->setHTMLAttribute('method', 'get');
        $this->setHTMLAttribute('id', 'loginForm');
        $this->addItem(\clarus\form_item_Item::create('email', \clarus\form_item_Item::TYPE_TEXT)->setLabel('email')
                        ->setHTMLAttribute('pattern', '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
                        ->setHTMLAttribute('required', 'required')
                        ->setHTMLAttribute('placeholder', _('nejaky@email.cz')));
        $this->addItem(\clarus\form_item_Item::create('pass', \clarus\form_item_Item::TYPE_PASS)->setLabel('pass')->setHTMLAttribute('required', 'required'));
        $this->addItem(\clarus\form_item_Item::create('budiz', \clarus\form_item_Item::TYPE_SUBMIT)->setDefaultValue(_('ok')));
    }

}