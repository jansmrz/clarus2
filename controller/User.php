<?php

class controller_User extends clarus\controller_Controller {

    public function _login() {
        $this->response->setContentType(clarus\Response::TYPE_JSON);

				$loginForm = new \component\LoginForm();

        if ($loginForm->onSubmit()) {
            if ($loginForm->isValid()) {
                try {
                    \clarus\security_User::login($loginForm->getValues()['email'], $loginForm->getValues()['pass']);
                    \clarus\Application::i()->redirect('/example');
                } catch (\clarus\security_autentification_Exception $sae) {
                    $loginForm->addFormError(_($sae->getMessage()));
                    header('HTTP/1.1 400 Bad Request');
                }
            }
        }
    }

}