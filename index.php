<?php

namespace clarus;

ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);

session_start();

try {

/*    include_once ('clarus' . DIRECTORY_SEPARATOR . 'Env.php');

    spl_autoload_register(function ($class) {
                $path = Env::i()->getPATH() . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, preg_split('~_|\\\~', $class)) . '.php';
                include_once($path);
            }, TRUE);

    Application::i()->addRoute(new router_Basic());
    Application::i()->start();
    Application::i()->render();*/

		include_once ('clarus' . DIRECTORY_SEPARATOR . 'Env.php');
		
		spl_autoload_register(function ($class) {
                $path = Env::i()->getPATH() . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, preg_split('~_|\\\~', $class)) . '.php';
                include_once($path);
            }, TRUE);

		Application::i()->addRoute(new router_Basic());
		Application::i()->start();
	 	Application::i()->render();

} catch (\Exception $e) {
    header('HTTP/1.1 500 Internal Server Error');
    if (Response::i()->getContentType() == Response::TYPE_JSON) {
        $responsePacket = ['_status' => ['msg' => $e->getMessage(), 'code' => $e->getCode()], '_data' => NULL];
        echo json_encode($responsePacket);
    } else {
        echo '<h1>Exception</h1>';
        echo '<pre>' . print_r($e, TRUE) . '</pre>';
    }
}